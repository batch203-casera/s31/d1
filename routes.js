const http = require("http");

// Creates a variable "port" to store the port number
const port = 4000;
	
// "request" and "response" is an object, with property and methods.
const server = http.createServer((req, res) => {
	// We will create two endpoint route for "/greeting" and "/homepage" and will return a response upon accessing.
	// The "url" property refers to the url or the link in the browser (endpoint).
	// baseURI (/), is "/" == "/greeting" (false)
	if(req.url == "/greeting"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Hello Again");
	}
	else if (req.url == "/homepage") {
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end('This is the homepage')
    }
	else{
		res.writeHead(404, {"Content-Type": "text/plain"});
		res.end("Page not available.");
	}

	// Mini activity
	// Create another endpoint for the "/homepage" and send a response "This is the homepage".
});

// Uses the "server" and "port" variables created above.
server.listen(port);

// When server is running, console will print the message:
console.log(`Server is now accessible at localhost:${port}`);